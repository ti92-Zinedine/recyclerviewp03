package com.example.recyclerviewp3;

import android.app.Application;
import android.util.Log;

import java.util.ArrayList;

import Modelo.AlumnosDb;

public class Aplicacion extends Application {
    static ArrayList<Alumno> alumnos;
    private AlumnoAdapter adaptador;
    private AlumnosDb alumnosDb;

    public ArrayList<Alumno> getAlumnos() { return alumnos; }
    public AlumnoAdapter getAdaptador() { return adaptador; }

    @Override
    public void onCreate() {
        super.onCreate();
        alumnosDb = new AlumnosDb(getApplicationContext());
        //alumnos = Alumno.llenarAlumnos();
        alumnos = alumnosDb.allAlumnos();
        //alumnosDb.openDataBase();

        adaptador = new AlumnoAdapter(alumnos, this);
        Log.d("", "onCreate: tamaño array list " + alumnos.size());
    }
}
