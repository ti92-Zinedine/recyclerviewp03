package com.example.recyclerviewp3;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class AlumnoAdapter extends RecyclerView.Adapter<AlumnoAdapter.ViewHolder> implements View.OnClickListener, Filterable{
    protected ArrayList<Alumno> listaAlumnos;
    private ArrayList<Alumno> nAlumnoFiltrado; // Variable para SearchView
    private View.OnClickListener listener;
    private Context context;
    private LayoutInflater inflater;

    public void actualizarListaAlumnos(ArrayList<Alumno> listaAlumnos) {
        this.listaAlumnos = listaAlumnos;
        this.nAlumnoFiltrado = new ArrayList<>(listaAlumnos);
        notifyDataSetChanged();
    }

    public AlumnoAdapter(ArrayList<Alumno> listaAlumnos, Context context) {
        this.listaAlumnos = listaAlumnos;
        this.context = context;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        this.nAlumnoFiltrado = new ArrayList<>(listaAlumnos); // Asignar la lista de alumnos completa
    }

    public void filtrar(String texto) {
        listaAlumnos.clear();

        if (texto.isEmpty()) {
            listaAlumnos.addAll(nAlumnoFiltrado);
        } else {
            texto = texto.toLowerCase();
            for (Alumno alumno : nAlumnoFiltrado) {
                // Verificar si el nombre del alumno contiene el texto de búsqueda
                if (alumno.getMatricula().toLowerCase().contains(texto) ||
                        alumno.getNombre().toLowerCase().contains(texto) ||
                        alumno.getCarrera().toLowerCase().contains(texto)) {
                    listaAlumnos.add(alumno); // Agregar alumno a la lista
                }
            }
        }

        notifyDataSetChanged(); // Notificar al RecyclerView que los datos fueron cambiados
    }

    @NonNull
    @NotNull
    @Override
    public AlumnoAdapter.ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.alumnos_items, null);
        view.setOnClickListener(this); //Escucha el evento click

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull AlumnoAdapter.ViewHolder holder, int position) {
        Alumno alumno = listaAlumnos.get(position);
        holder.txtMatricula.setText(alumno.getMatricula());
        holder.txtNombre.setText(alumno.getNombre());
        holder.txtCarrera.setText(alumno.getCarrera());

        if (alumno.getImg() != null) {
            holder.idImagen.setImageURI(Uri.parse(alumno.getImg()));
        } else {
            // Establecer una imagen de reemplazo o dejarlo vacío según sea necesario
            holder.idImagen.setImageDrawable(null);
        }

    }

    @Override
    public int getItemCount() { return listaAlumnos != null ? listaAlumnos.size() : 0; }

    public void setOnClickListener(View.OnClickListener listener) { this.listener = listener; }

    @Override
    public void onClick(View v) { if(listener != null) listener.onClick(v); }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();
                ArrayList<Alumno> filteredList = new ArrayList<>();

                if (constraint == null || constraint.length() == 0) {
                    filteredList.addAll(nAlumnoFiltrado);
                } else {
                    String filterPattern = constraint.toString().toLowerCase().trim();
                    for (Alumno alumnoF : nAlumnoFiltrado) {
                        if (alumnoF.getMatricula().toLowerCase().contains(filterPattern) ||
                                alumnoF.getNombre().toLowerCase().contains(filterPattern) ||
                                alumnoF.getCarrera().toLowerCase().contains(filterPattern)) {
                            filteredList.add(alumnoF);
                        }
                    }
                }

                results.values = filteredList;
                results.count = filteredList.size();
                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                listaAlumnos.clear();
                listaAlumnos.addAll((ArrayList<Alumno>) results.values);
                notifyDataSetChanged();
            }
        };
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private LayoutInflater inflater;
        private TextView txtNombre;
        private TextView txtMatricula;
        private TextView txtCarrera;
        private ImageView idImagen;
        public ViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            txtNombre = (TextView) itemView.findViewById(R.id.txtAlumnoNombre);
            txtMatricula = (TextView) itemView.findViewById(R.id.txtMatricula);
            txtCarrera = (TextView) itemView.findViewById(R.id.txtCarrera);
            idImagen = (ImageView) itemView.findViewById(R.id.foto);
        }
    }
}